// vue.config.js
module.exports = {
    publicPath: '/admin',
    devServer: {
        port: 8080,
        disableHostCheck: true,
        proxy: {
            '^/api': {
                //本地服务器地址
                target: 'http://localhost:9000/',
                changeOrigin: true, // 将主机标头的原点更改为目标URL
                pathRewrite: {
                    '^/api': '/' //代理的路径
                }
            },
        }
    },
}